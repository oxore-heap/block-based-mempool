CFLAGS=-MD -ggdb3 $(EXTRACFLAGS)
LDFLAGS=$(EXTRALDFLAGS)

main: main.o mempool.o

clean:
	rm -rfv *.o *.d main

-include main.d mempool.d
