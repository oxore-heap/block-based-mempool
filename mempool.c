#include <stdio.h>
#include <string.h>

#include "mempool.h"

static inline void init_bitmap(
        uint8_t *bitmap,
        size_t bitmap_size,
        size_t blocks_count)
{
    if (blocks_count % 8) {
        bitmap[bitmap_size-1] = (uint8_t)(UINT8_MAX << (blocks_count % 8));
    }
}

/* Returns least significant bit that has *not* been set to 1.
 * Returns 0 if all bits are set to 1. */
static inline ssize_t lsb_zero(uint8_t byte)
{
    for (size_t i = 0; i < sizeof(byte) * 8; i++, byte >>= 1) {
        if ((uint8_t)(byte & 1) == 0) {
            return i;
        }
    }

    return -1;
}

bool mempool_is_empty(const struct mempool *mempool)
{
    if (mempool == NULL) {
        return false;
    }

    uint8_t *bitmap = mempool->bitmap;
    size_t blocks_count = mempool->blocks_count;

    for (size_t i = 0; i < blocks_count; i+=8) {
        uint8_t byte = bitmap[i/8];
        if (byte != 0) {
            if (blocks_count - i < 8) {
                if (byte != (uint8_t)(UINT8_MAX << (blocks_count - i))) {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    return true;
}

bool mempool_is_full(const struct mempool *mempool)
{
    if (mempool == NULL) {
        return false;
    }

    uint8_t *bitmap = mempool->bitmap;
    size_t blocks_count = mempool->blocks_count;

    for (size_t i = 0; i < blocks_count; i+=8) {
        if (lsb_zero(bitmap[i/8]) != -1) {
            return false;
        }
    }

    return true;
}

void mempool_init(struct mempool *mempool, void *mem, size_t mem_size)
{
    if (mem == NULL || mem == NULL || mem_size < CFG_MEMPOOL_BLOCK_SIZE + 1) {
        return;
    }

    *mempool = (struct mempool){0};
    memset(mem, 0, mem_size);

    /* For every eight blocks one byte should be reserved for bitmap. Remaining
     * space can surely fit less than eight blocks plus one byte,but it can be
     * zero. So if that remaining space cannot fit at least one block plus one
     * byte, then it is not interesting at all and not going to be used. */

    size_t blocks_count = mem_size / (CFG_MEMPOOL_BLOCK_SIZE * 8 + 1) * 8;
    size_t bitmap_size = blocks_count / 8;
    size_t remaining = mem_size % (CFG_MEMPOOL_BLOCK_SIZE * 8 + 1);

    if (remaining >= CFG_MEMPOOL_BLOCK_SIZE + 1) {
        bitmap_size++;
        blocks_count += (remaining - 1) / CFG_MEMPOOL_BLOCK_SIZE;
    }

    mempool->pool = mem;
    mempool->blocks_count = blocks_count;

    mempool->bitmap_size = bitmap_size;
    mempool->bitmap = &((uint8_t *)mem)[mem_size - bitmap_size];

    init_bitmap(mempool->bitmap, bitmap_size, blocks_count);
}

void *mempool_acquire(struct mempool *mempool)
{
    if (mempool == NULL) {
        return NULL;
    }

    uint8_t *pool = mempool->pool;
    uint8_t *bitmap = mempool->bitmap;
    size_t blocks_count = mempool->blocks_count;
    void *block = NULL;

    for (size_t i = 0; i < blocks_count; i+=8) {
        ssize_t bit_num = lsb_zero(bitmap[i/8]);
        if (bit_num != -1) {
            block = &pool[(i + bit_num) * CFG_MEMPOOL_BLOCK_SIZE];
            bitmap[i/8] |= (uint8_t)(1 << bit_num);
            break;
        }
    }

    return block;
}

void mempool_release(struct mempool *mempool, void *block)
{
    if (mempool == NULL) {
        return;
    }

    uint8_t *pool = mempool->pool;
    uint8_t *bitmap = mempool->bitmap;
    size_t blocks_count = mempool->blocks_count;

    for (size_t i = 0; i < blocks_count; i++) {
        if (&pool[i*CFG_MEMPOOL_BLOCK_SIZE] == block) {
            bitmap[i/8] &= ~(uint8_t)(1 << (i % 8));
            memset(&pool[i], 0, CFG_MEMPOOL_BLOCK_SIZE);
            return;
        }
    }
}
