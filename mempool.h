#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifndef CFG_MEMPOOL_BLOCK_SIZE
/**
 * Size of any block in memory pool, can be redefined
 * */
#   define CFG_MEMPOOL_BLOCK_SIZE 16
#endif

struct mempool {
    void *pool;
    size_t blocks_count;
    uint8_t *bitmap;
    size_t bitmap_size;
};

typedef struct mempool mempool_t;

/**
 * \brief Checks whether mempool is completely free or not
 * \param mempool Pointer to a memory pool
 * \retval false mempool has at least one occupied block
 * \retval true All blocks in mempool are ready to be occupied
 * */
bool mempool_is_empty(const struct mempool *mempool);

/**
 * \brief Checks whether mempool is completely occupied or not
 * \param mempool Pointer to a memory pool
 * \retval false mempool has at least one unoccupied block
 * \retval true All blocks in mempool are occupied
 * */
bool mempool_is_full(const struct mempool *mempool);

/**
 * \brief Initialized memory pool which is necessary to make further actions
 * \param mempool Pointer to a memory pool
 * \param mem Pointer to start of continuous memory region that is going to be
 *  used for memory pool
 * \param mem_size Size in bytes of memory region pointed by mem
 * */
void mempool_init(struct mempool *mempool, void *mem, size_t mem_size);

/**
 * \brief Acquire a block from memory pool
 * \param mempool Pointer to a memory pool
 * \retval NULL mempool has no free blocks hence block has not been acquired
 * \retval ptr Pointer to acquired block from mempool is returned
 * */
void *mempool_acquire(struct mempool *mempool);

/**
 * \brief Give a block back to memory pool
 * \param mempool Pointer to a memory pool
 * \param block Pointer to the block is being released
 * */
void mempool_release(struct mempool *mempool, void *block);
