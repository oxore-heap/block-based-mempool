#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "mempool.h"

#define MEM_SIZE_8_BLOCKS (CFG_MEMPOOL_BLOCK_SIZE * 8 + 1)
void test_acquire_8_blocks(void)
{
    struct mempool mempool = {0};
    char mem[MEM_SIZE_8_BLOCKS];

    mempool_init(&mempool, mem, MEM_SIZE_8_BLOCKS);

    assert(mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));

    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*0] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*1] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*2] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*3] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*4] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*5] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*6] == mempool_acquire(&mempool));
    assert(!mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*7] == mempool_acquire(&mempool));

    assert(NULL == mempool_acquire(&mempool));
    assert(!mempool_is_empty(&mempool));
    assert(mempool_is_full(&mempool));

    printf("%s: OK\n", __func__);
}

#define MEM_SIZE_9_BLOCKS (CFG_MEMPOOL_BLOCK_SIZE * 9 + 2)
void test_acquire_9_blocks(void)
{
    struct mempool mempool = {0};
    char mem[MEM_SIZE_9_BLOCKS];

    mempool_init(&mempool, mem, MEM_SIZE_9_BLOCKS);

    assert(mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));

    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*0] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*1] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*2] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*3] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*4] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*5] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*6] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*7] == mempool_acquire(&mempool));
    assert(!mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*8] == mempool_acquire(&mempool));

    assert(NULL == mempool_acquire(&mempool));
    assert(!mempool_is_empty(&mempool));
    assert(mempool_is_full(&mempool));

    printf("%s: OK\n", __func__);
}

#define MEM_SIZE_8_BLOCKS_SLIGHTLY_RELAXED (CFG_MEMPOOL_BLOCK_SIZE * 8 + 2)
void test_acquire_8_blocks_slightly_relaxed(void)
{
    struct mempool mempool = {0};
    char mem[MEM_SIZE_8_BLOCKS_SLIGHTLY_RELAXED];

    mempool_init(&mempool, mem, MEM_SIZE_8_BLOCKS_SLIGHTLY_RELAXED);

    assert(mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));

    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*0] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*1] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*2] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*3] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*4] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*5] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*6] == mempool_acquire(&mempool));
    assert(!mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*7] == mempool_acquire(&mempool));

    assert(NULL == mempool_acquire(&mempool));
    assert(!mempool_is_empty(&mempool));
    assert(mempool_is_full(&mempool));

    printf("%s: OK\n", __func__);
}

#define MEM_SIZE_8_BLOCKS_RELAXED_ALOT (CFG_MEMPOOL_BLOCK_SIZE * 9 + 1)
void test_acquire_8_blocks_relaxed_alot(void)
{
    struct mempool mempool = {0};
    char mem[MEM_SIZE_8_BLOCKS_RELAXED_ALOT];

    mempool_init(&mempool, mem, MEM_SIZE_8_BLOCKS_RELAXED_ALOT);

    assert(mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));

    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*0] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*1] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*2] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*3] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*4] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*5] == mempool_acquire(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*6] == mempool_acquire(&mempool));
    assert(!mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*7] == mempool_acquire(&mempool));

    assert(NULL == mempool_acquire(&mempool));
    assert(!mempool_is_empty(&mempool));
    assert(mempool_is_full(&mempool));

    printf("%s: OK\n", __func__);
}

void test_acquire_release_and_then_acquire_8_of_8_blocks(void)
{
    struct mempool mempool = {0};
    char mem[MEM_SIZE_8_BLOCKS];
    char *blocks[8] = {0};

    mempool_init(&mempool, mem, MEM_SIZE_8_BLOCKS);

    /* Acquire 8 of 8 */

    assert(mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));

    blocks[0] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*0] == blocks[0]);
    blocks[1] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*1] == blocks[1]);
    blocks[2] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*2] == blocks[2]);
    blocks[3] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*3] == blocks[3]);
    blocks[4] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*4] == blocks[4]);
    blocks[5] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*5] == blocks[5]);
    blocks[6] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*6] == blocks[6]);
    assert(!mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));
    blocks[7] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*7] == blocks[7]);

    assert(NULL == mempool_acquire(&mempool));
    assert(!mempool_is_empty(&mempool));
    assert(mempool_is_full(&mempool));

    /* Release 8 of 8 */

    mempool_release(&mempool, blocks[0]);
    mempool_release(&mempool, blocks[1]);
    mempool_release(&mempool, blocks[2]);
    mempool_release(&mempool, blocks[3]);
    mempool_release(&mempool, blocks[4]);
    mempool_release(&mempool, blocks[5]);
    mempool_release(&mempool, blocks[6]);
    mempool_release(&mempool, blocks[7]);

    memset(blocks, 0, sizeof(blocks));

    /* Acquire 8 of 8 */

    assert(mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));

    blocks[0] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*0] == blocks[0]);
    blocks[1] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*1] == blocks[1]);
    blocks[2] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*2] == blocks[2]);
    blocks[3] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*3] == blocks[3]);
    blocks[4] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*4] == blocks[4]);
    blocks[5] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*5] == blocks[5]);
    blocks[6] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*6] == blocks[6]);
    assert(!mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));
    blocks[7] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*7] == blocks[7]);

    assert(NULL == mempool_acquire(&mempool));
    assert(!mempool_is_empty(&mempool));
    assert(mempool_is_full(&mempool));

    printf("%s: OK\n", __func__);
}

void test_acquire_8_release_1_and_then_acquire_1_of_8_blocks(void)
{
    struct mempool mempool = {0};
    char mem[MEM_SIZE_8_BLOCKS];
    char *blocks[8] = {0};

    mempool_init(&mempool, mem, MEM_SIZE_8_BLOCKS);

    /* Acquire 8 of 8 */

    assert(mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));

    blocks[0] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*0] == blocks[0]);
    blocks[1] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*1] == blocks[1]);
    blocks[2] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*2] == blocks[2]);
    blocks[3] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*3] == blocks[3]);
    blocks[4] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*4] == blocks[4]);
    blocks[5] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*5] == blocks[5]);
    blocks[6] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*6] == blocks[6]);
    blocks[7] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*7] == blocks[7]);

    assert(NULL == mempool_acquire(&mempool));
    assert(!mempool_is_empty(&mempool));
    assert(mempool_is_full(&mempool));

    /* Release 1 */

    mempool_release(&mempool, blocks[0]);
    assert(!mempool_is_empty(&mempool));
    assert(!mempool_is_full(&mempool));

    /* Acquire 1 */

    blocks[0] = mempool_acquire(&mempool);
    assert(&mem[CFG_MEMPOOL_BLOCK_SIZE*0] == blocks[0]);

    assert(NULL == mempool_acquire(&mempool));
    assert(!mempool_is_empty(&mempool));
    assert(mempool_is_full(&mempool));

    printf("%s: OK\n", __func__);
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;

    test_acquire_8_blocks();
    test_acquire_9_blocks();
    test_acquire_8_blocks_slightly_relaxed();
    test_acquire_8_blocks_relaxed_alot();
    test_acquire_release_and_then_acquire_8_of_8_blocks();
    test_acquire_8_release_1_and_then_acquire_1_of_8_blocks();
}
